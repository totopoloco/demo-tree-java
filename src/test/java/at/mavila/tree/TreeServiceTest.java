package at.mavila.tree;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class TreeServiceTest {

  private final TreeService treeService;


  @Autowired
  public TreeServiceTest(final TreeService treeService) {
    this.treeService = treeService;
  }

  @Test
  void addRootNode() {

    Person soltera = Person.builder().name("soltera").age(67).build();
    TreeNode root = this.treeService.constructTree(List.of(soltera));
    assertThat(root.person().getName()).isEqualTo("soltera");
    assertThat(root.left()).isNull();
    assertThat(root.right()).isNull();
  }

  @Test
  void addLeftNode() {
    Person abuelita = Person.builder().name("abuelita").age(67).build();
    Person ismael = Person.builder().name("ismael").age(3).build();

    TreeNode treeNode = this.treeService.constructTree(List.of(abuelita, ismael));

    assertThat(treeNode.person().getName()).isEqualTo("abuelita");
    assertThat(treeNode.left().person().getName()).isEqualTo("ismael"); //ismael is the left node
  }

  @Test
  void addTwoOrderedChildrenNodes() {
    Person abuelita = Person.builder().name("abuelita").age(67).build();
    Person ismael = Person.builder().name("ismael").age(3).build();
    Person rafael = Person.builder().name("rafael").age(5).build();

    TreeNode treeNode = this.treeService.constructTree(List.of(abuelita, ismael, rafael));

    assertThat(treeNode.person().getName()).isEqualTo("abuelita");
    assertThat(treeNode.left().person().getName()).isEqualTo("ismael"); //ismael is the left node
    assertThat(treeNode.right().person().getName()).isEqualTo("rafael"); //rafael is the right node
  }

  @Test
  void addTwoUnorderedChildrenNodes() {
    Person madre = Person.builder().name("madre").age(67).build();
    Person ismael = Person.builder().name("ismael").age(3).build();
    Person rafael = Person.builder().name("rafael").age(5).build();

    TreeNode treeNode = this.treeService.constructTree(List.of(madre, rafael, ismael));

    assertThat(treeNode.person().getName()).isEqualTo("madre");
    assertThat(treeNode.left().person().getName()).isEqualTo("ismael"); //ismael is the left node
    assertThat(treeNode.right().person().getName()).isEqualTo("rafael"); //rafael is the right node
  }

  @Test
  void addManuallyChildrenNodes() {
    Person abuelita = Person.builder().name("abuelita").age(67).gender(Gender.FEMALE).build();
    Person ismael = Person.builder().name("ismael").age(40).gender(Gender.MALE).build();
    Person rafael = Person.builder().name("rafael").age(43).gender(Gender.MALE).build();
    Person angelica = Person.builder().name("angelica").age(5).gender(Gender.FEMALE).build();
    Person israel = Person.builder().name("israel").age(3).gender(Gender.MALE).build();
    Person stephan = Person.builder().name("stephan").age(6).gender(Gender.MALE).build();
    Person remy = Person.builder().name("remy").age(5).gender(Gender.MALE).build();

    TreeNode root = this.treeService.addBinaryNode(null, abuelita);

    TreeNode treeNodeIsmael = this.treeService.addBinaryNode(null, ismael);
    treeNodeIsmael = this.treeService.addBinaryNode(treeNodeIsmael, angelica);
    treeNodeIsmael = this.treeService.addBinaryNode(treeNodeIsmael, israel);

    TreeNode treeNodeRafael = this.treeService.addBinaryNode(null, rafael);
    treeNodeRafael = this.treeService.addBinaryNode(treeNodeRafael, stephan);
    treeNodeRafael = this.treeService.addBinaryNode(treeNodeRafael, remy);

    root = this.treeService.appendBinaryNode(root, treeNodeIsmael);
    root = this.treeService.appendBinaryNode(root, treeNodeRafael);

    this.treeService.showTreeAsIs(root);

    assertThat(root.person().getName()).isEqualTo("abuelita");
    assertThat(root.left().person().getName()).isEqualTo("ismael"); //ismael is the left node
    assertThat(root.right().person().getName()).isEqualTo("rafael"); //rafael is the right node
  }

  @Test
  void addManuallyChildrenNodesAndTraverse() {
    Person abuelita = Person.builder().name("abuelita").age(67).gender(Gender.FEMALE).build();
    Person ismael = Person.builder().name("ismael").age(40).gender(Gender.MALE).build();
    Person rafael = Person.builder().name("rafael").age(43).gender(Gender.MALE).build();
    Person angelica = Person.builder().name("angelica").age(5).gender(Gender.FEMALE).build();
    Person israel = Person.builder().name("israel").age(3).gender(Gender.MALE).build();

    TreeNode root = this.treeService.addBinaryNode(null, abuelita);

    TreeNode treeNodeIsmael = this.treeService.addBinaryNode(null, ismael);
    treeNodeIsmael = this.treeService.addBinaryNode(treeNodeIsmael, angelica);
    treeNodeIsmael = this.treeService.addBinaryNode(treeNodeIsmael, israel);

    root = this.treeService.appendBinaryNode(root, treeNodeIsmael);
    root = this.treeService.addBinaryNode(root, rafael);

    this.treeService.showTreeAsIs(root);
    log.info("Applying swapping");
    TreeNode traverse = this.treeService.mirror(root);
    this.treeService.showTreeAsIs(traverse);
    assertThat(traverse.person().getName()).isEqualTo("abuelita");

    assertThat(root).isNotEqualTo(traverse);

  }

}