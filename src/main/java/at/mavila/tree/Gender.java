package at.mavila.tree;

public enum Gender {
  FEMALE, MALE, UNDECIDED, OTHER
}
