package at.mavila.tree;

import lombok.Builder;
import lombok.With;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@With
@Builder
public record TreeNode(TreeNode left, TreeNode right, Person person) {

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
        .append("left", left)
        .append("right", right)
        .append("person", person)
        .toString();
  }
}
