package at.mavila.tree;

import static java.util.Objects.isNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.With;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
@With
@Builder
@AllArgsConstructor
public class Person implements Comparable<Person> {
  private String name;
  private Integer age;
  private Gender gender;


  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
        .append("name", this.name)
        .append("age", this.age)
        .append("gender", this.gender)
        .toString();
  }


  /**
   * Compares this object with the specified object for order.
   * Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
   * Using the age as the first criteria and the name as the second criteria.
   *
   * @param thatPerson the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object.
   */
  @Override
  public int compareTo(final Person thatPerson) {

    Optional<Integer> compareByAge = validateAge(thatPerson);
    if (compareByAge.isPresent()) {
      return compareByAge.get();
    }

    if (this.getAge().compareTo(thatPerson.getAge()) == 0) {
      return compareByName(thatPerson);
    }

    return this.getAge().compareTo(thatPerson.getAge());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Person person = (Person) o;

    return new EqualsBuilder()
        .append(getName(), person.getName())
        .append(getAge(), person.getAge())
        .append(getGender(), person.getGender())
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(getName()).append(getAge()).append(getGender()).toHashCode();
  }

  private Optional<Integer> validateAge(Person thatPerson) {
    if (isNull(this.getAge()) && isNull(thatPerson.getAge())) {
      return of(0);
    }

    if (isNull(this.getAge())) {
      return of(-1);
    }

    if (isNull(thatPerson.getAge())) {
      return of(1);
    }

    return empty();
  }

  private int compareByName(Person thatPerson) {
    //Compare considering null values in the name
    if (isNull(this.getName()) && isNull(thatPerson.getName())) {
      return 0;
    }

    if (isNull(this.getName())) {
      return -1;
    }

    if (isNull(thatPerson.getName())) {
      return 1;
    }

    return this.getName().compareTo(thatPerson.getName());
  }
}
