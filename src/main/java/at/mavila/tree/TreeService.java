package at.mavila.tree;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TreeService {

  //Method to construct a tree from a given list of persons (not an array).
  //The compareTo method is used to properly place the person in the tree.
  //The tree is constructed by comparing the person to the root node and then
  //to the left and right nodes of the root node.
  public TreeNode constructTree(List<Person> persons) {
    AtomicReference<TreeNode> rootAtomicReference = new AtomicReference<>(null);
    persons.stream().filter(Objects::nonNull)
        .forEach(person -> rootAtomicReference.set(addBinaryNode(rootAtomicReference.get(), person)));
    return rootAtomicReference.get();
  }


  /**
   * If there is no root node, the person is added as the root node.
   * If there is a root node, the person is compared to the root node and
   * added to the left or right node of the root node following these rules:
   * If there is no left node, the person is added as the left node.
   * If there is a left node, the person is compared to the left node and
   * move the left node to the right node of the left node if the person is
   * greater than the left node.
   *
   * @param root   the root node.
   * @param person the person to be added.
   * @return a TreeNode with children.
   */
  public TreeNode addBinaryNode(TreeNode root, Person person) {

    if (Objects.isNull(root)) {
      return TreeNode.builder().person(person).build();
    }

    TreeNode left = root.left();
    if (Objects.isNull(left)) {
      return root.withLeft(TreeNode.builder().person(person).build());
    }

    final Person personLeft = left.person();

    if (personLeft.compareTo(person) <= 0) {
      return root.withRight(TreeNode.builder().person(person).build());
    }

    return root.withLeft(TreeNode.builder().person(person).build()).withRight(TreeNode.builder().person(personLeft).build());
  }

  //Method to append a TreeNode to a given TreeNode.
  public TreeNode appendBinaryNode(TreeNode root, TreeNode child) {
    if (Objects.isNull(root)) {
      return child;
    }

    TreeNode left = root.left();
    if (Objects.isNull(left)) {
      return root.withLeft(child);
    }

    TreeNode right = root.right();
    if (Objects.isNull(right)) {
      return root.withRight(child);
    }

    return root.withLeft(left).withRight(appendBinaryNode(right, child));
  }

  public void showTreeAsIs(TreeNode root) {
    if (Objects.isNull(root)) {
      return;
    }
    final TreeNode left = root.left();
    showTreeAsIs(left);
    if (Objects.nonNull(left)) {
      final Person personLeft = left.person();
      if (StringUtils.isNotBlank(personLeft.getName())) {
        log.info("Left {}", personLeft);
      }
    }

    logTopNode(root);

    final TreeNode right = root.right();
    if (Objects.nonNull(right)) {
      final Person personRight = right.person();
      if (StringUtils.isNotBlank(personRight.getName())) {
        log.info("Right {}", personRight);
        log.info("------------------------------------------------------------------------");
      }
    }

    showTreeAsIs(right);
  }

  /**
   * Method to mirror a tree.
   * An Inversion of a Binary Tree is an operation where the left and right children (of all non-leaf nodes) of it are swapped.
   *
   * @param root where to start the inversion.
   * @return a TreeNode with children mirrored.
   */
  public TreeNode mirror(TreeNode root) { // Each child of a tree is a root of its subtree.
    if (Objects.isNull(root)) {
      return null;
    }

    final TreeNode currentLeft = root.left();
    final TreeNode currentRight = root.right();

    if (Objects.isNull(currentLeft) && Objects.isNull(currentRight)) {
      return root;
    }

    if (Objects.isNull(currentLeft)) {
      return root.withLeft(currentRight);
    }
    //Swap left for right node
    return root.withLeft(currentRight).withRight(mirror(currentLeft));

  }

  private static void logTopNode(TreeNode root) {
    if ((Objects.isNull(root.left()) && Objects.isNull(root.right()))) {
      log.info("No descendents for {}", root.person());
      log.info("------------------------------------------------------------------------");
      return;
    }
    log.info("Person {}", root.person());

  }


}
